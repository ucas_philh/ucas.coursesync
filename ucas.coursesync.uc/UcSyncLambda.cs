﻿using Amazon.Lambda.SQSEvents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ucas.CourseSync.CourseIntegration.Db.Repositories;
using Ucas.CourseSync.Uc.Logging;

namespace Ucas.CourseSync.Uc
{
    public class UcSyncLambda
    {
        private readonly IReferenceDataRepository _referenceDataRepository;

        public UcSyncLambda(IReferenceDataRepository referenceDataRepository)
        {
            _referenceDataRepository = referenceDataRepository;
        }
        public async Task ProcessCourseMessageAsync(SQSEvent.SQSMessage message, Logger logger)
        {
            try
            {
                var test = await _referenceDataRepository.AdmissionTest("1");
                
                logger.LogLine(@"Processing course message start {test.AdmissionTestCode}");
               
            }
            catch (Exception ex)
            {
                logger.LogLine(ex.Message);
                logger.LogLine(ex.StackTrace);
                throw;
            }
        }
    }
}
