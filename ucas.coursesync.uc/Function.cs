using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using Amazon.Lambda.Core;
using Amazon.Lambda.SQSEvents;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Ucas.AspNetCore.IdentityMiddleware.Startup;
using Ucas.AspNetCore.IdentityMiddleware.SystemJwt;
using Ucas.CourseSync.CourseIntegration.Db.Context;
using Ucas.CourseSync.CourseIntegration.Db.Repositories;
using Ucas.CourseSync.Uc;
using Ucas.CourseSync.Uc.Logging;
using Ucas.CourseSync.Uc.Services;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace ucas.coursesync.uc
{
    public class Function
    {
        private const string SystemId = "35adb837-06c7-4965-8daf-f213617967b8";
        private const string SystemName = "UcCourseSync";

        private static IConfiguration _configuration;
        private static ServiceCollection _serviceCollection;

        /// <summary>
        /// A simple function that takes a string and does a ToUpper
        /// </summary>
        /// <param name="input"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task FunctionHandler(SQSEvent evnt, ILambdaContext context)
        {
            var ucSyncLambda = SetUpServices(context);
            await HandleSQSMessageAsync(evnt, context, ucSyncLambda.ProcessCourseMessageAsync);
        }

        /// <summary>
        /// This method is called for every Lambda invocation. This method takes in an SQS event object and can be used
        /// to respond to SQS messages.
        /// </summary>
        /// <param name="evnt"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        async Task HandleSQSMessageAsync(SQSEvent evnt, ILambdaContext context, Func<SQSEvent.SQSMessage, Logger, Task> ucSyncLambdaMethod)
        {
            try
            {
                foreach (var message in evnt.Records)
                {
                    var logger = new Logger(context.Logger, message.MessageId);

                    try
                    {
                        await ucSyncLambdaMethod(message, logger);
                    }
                    catch (Exception e)
                    {
                        logger.LogLine(e.Message);
                        logger.LogLine(e.StackTrace);
                        throw;
                    }
                }
            }
            catch (Exception e)
            {
                context.Logger.LogLine(e.Message);
                context.Logger.LogLine(e.StackTrace);
                throw;
            }
        }
        UcSyncLambda SetUpServices(ILambdaContext context)
        {
            try
            {
                InitiateContainer();
                ConfigureServices();
                var serviceProvider = _serviceCollection.BuildServiceProvider();
                return serviceProvider.GetRequiredService<UcSyncLambda>();
            }
            catch (Exception e)
            {
                context.Logger.LogLine(e.Message);
                context.Logger.LogLine(e.StackTrace);
                throw;
            }
        }
        private static void InitiateContainer()
        {
            _configuration = new ConfigurationBuilder()
                                .SetBasePath(Directory.GetCurrentDirectory())
                                .AddJsonFile("appsettings.json")
                                .AddJsonFile("appsettings.development.json", optional: true)
                                .Build();

            _serviceCollection = new ServiceCollection();
        }
        private static void ConfigureServices()
        {
            _serviceCollection
                .AddSingleton<UcSyncLambda>()
                ;

            // Add Identity services:
            _serviceCollection
                .AddUcasIdentityServices()
                .ConfigureIdentityClient(opts => opts.IdentityApiBaseUrl = _configuration["ServiceUrls:Identity"])
                .AddUcasIdentityJwtAuthentication() // middleware from Ucas.AspNetCore.IdentityMiddleware for jwt - based authentication & basic authorisation
                .AddUcasIdentityJwtBasicJwtAuthorisationPolicy()
                .AddSystemJwtServices() // services for fetching and using a system jwt for making calls to DA apis
                .ConfigureHmacAuthentication(_configuration.GetSection("HmacAuthentication"))
                .WithSharedSystemJwtAuthenticationHandlerFor(new SystemJwtIdentity
                {
                    SystemId = Guid.Parse(SystemId),
                    SubSystemId = SystemId,
                    SystemName = SystemName
                });

            // Add Courses client:
            _serviceCollection
                .AddHttpClient<ICoursesApiClient, CoursesApiClient>(client => client.BaseAddress = new Uri(_configuration["ServiceUrls:Courses"]))
            //    .WithAutomaticSystemJwtAuthorisation()
            ;

            // Add Db
            _serviceCollection
                .AddDbContext<CourseIntegrationContext>(options => 
                    options.UseOracle(_configuration["ConnectionStrings:db"], opt => opt.UseOracleSQLCompatibility("11")))
                .AddScoped<IReferenceDataRepository, ReferenceDataRepository>();
        }
    }
}
