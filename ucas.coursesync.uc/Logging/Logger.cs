﻿using Amazon.Lambda.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ucas.CourseSync.Uc.Logging
{
    public class Logger
    {
        private readonly ILambdaLogger _lambdaLogger;
        private readonly string _messageId;

        public Logger(ILambdaLogger lambdaLogger, string messageId) =>
            (_lambdaLogger, _messageId) = (lambdaLogger, messageId);

        public void LogLine(string entry) =>
            _lambdaLogger.LogLine($"[MessageId: {_messageId}] {entry}");
    }
}
