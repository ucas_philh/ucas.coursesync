﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Ucas.CourseSync.Uc.Services
{
    public interface ICoursesApiClient
    {
        //Task<Course> GetCourseAsync(string id, int academicYear, Logger logger);
        //Task<Provider> GetProviderAsync(string id, Logger logger);

        Task<string> Test();
    }
    public class CoursesApiClient : ICoursesApiClient
    {
        private readonly HttpClient _httpClient;

        public CoursesApiClient(HttpClient client)
        {
            _httpClient = client;
        }

        public Task<string> Test()
        {
            throw new NotImplementedException();
        }



        //    public async Task<Course> GetCourseAsync(string id, int academicYear, Logger logger)
        //    {
        //        logger.LogLine($"Fetching course {id}");

        //        Course course;

        //        using var request = new HttpRequestMessage(HttpMethod.Get, $"api/v1/course/{id}");
        //        request.Headers.Add("X-Ucas-Expand-Ref-Data", "true");
        //        using var response = await _httpClient.SendAsync(request);

        //        logger.LogLine($"Fetching Course {id} returned with a status code of {response.StatusCode}");

        //        if (response.IsSuccessStatusCode)
        //        {
        //            course = await response.Content.ReadAsAsync<Course>();
        //            logger.LogLine($"Course {id} fetched: {JsonConvert.SerializeObject(course)}");
        //        }
        //        else
        //        {
        //            throw new Exception("course not found");
        //        }

        //        return course;
        //    }

        //    public async Task<Provider> GetProviderAsync(string id, Logger logger)
        //    {
        //        logger.LogLine($"Fetching provider {id}");

        //        Provider provider;
        //        using var request = new HttpRequestMessage(HttpMethod.Get, $"api/v1/providers/{id}?enrich=false");
        //        request.Headers.Add("UcasPermissionsDomain", "CollectProviders");

        //        using var response = await _httpClient.SendAsync(request);
        //        logger.LogLine($"Fetching Provider {id} returned with a status code of {response.StatusCode}");

        //        if (response.IsSuccessStatusCode)
        //        {
        //            provider = await response.Content.ReadAsAsync<Provider>();
        //            logger.LogLine($"Provider {id} fetched: {JsonConvert.SerializeObject(provider)}");
        //        }
        //        else
        //        {
        //            throw new Exception("provder not found");
        //        }

        //        return provider;
        //    }
        //}
    }
}
