﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Ucas.CourseSync.CourseIntegration.Db.Entities;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Ucas.CourseSync.CourseIntegration.Db.Context
{
    public partial class CourseIntegrationContext : DbContext
    {
        public CourseIntegrationContext()
        {
        }

        public CourseIntegrationContext(DbContextOptions<CourseIntegrationContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CiAudition> CiAudition { get; set; }
        public virtual DbSet<CiCourse> CiCourse { get; set; }
        public virtual DbSet<CiCourseInstrument> CiCourseInstrument { get; set; }
        public virtual DbSet<CiCourseJointInstrument> CiCourseJointInstrument { get; set; }
        public virtual DbSet<CiCourseOption> CiCourseOption { get; set; }
        public virtual DbSet<CiCourseStudyType> CiCourseStudyType { get; set; }
        public virtual DbSet<CiProvider> CiProvider { get; set; }
        public virtual DbSet<CiProviderContact> CiProviderContact { get; set; }
        public virtual DbSet<CiProviderFee> CiProviderFee { get; set; }
        public virtual DbSet<CiProviderLocation> CiProviderLocation { get; set; }
        public virtual DbSet<MapAttAdmissionTestType> MapAttAdmissionTestType { get; set; }
        public virtual DbSet<MapCourseType> MapCourseType { get; set; }
        public virtual DbSet<MapQltQlfType> MapQltQlfType { get; set; }
        public virtual DbSet<MapRegRegion> MapRegRegion { get; set; }
        public virtual DbSet<MapStmStudyMode> MapStmStudyMode { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseOracle("User Id=courseintegration;Password=courseintegration;Data Source=localhost:15211/xe");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:DefaultSchema", "COURSEINTEGRATION");

            modelBuilder.Entity<CiAudition>(entity =>
            {
                entity.HasKey(e => new { e.CourseId, e.AuditionMode })
                    .HasName("CI_AUDITION_PK");

                entity.ToTable("CI_AUDITION");

                entity.Property(e => e.CourseId)
                    .HasColumnName("COURSE_ID")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.AuditionMode)
                    .HasColumnName("AUDITION_MODE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CukasCollectFlag)
                    .HasColumnName("CUKAS_COLLECT_FLAG")
                    .HasColumnType("CHAR(1)");

                entity.Property(e => e.FeeLate1)
                    .HasColumnName("FEE_LATE_1")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.FeeLate2)
                    .HasColumnName("FEE_LATE_2")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.FeeOntime1)
                    .HasColumnName("FEE_ONTIME_1")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.FeeOntime2)
                    .HasColumnName("FEE_ONTIME_2")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.LastUpdated)
                    .HasColumnName("LAST_UPDATED")
                    .HasColumnType("TIMESTAMP(6)");
            });

            modelBuilder.Entity<CiCourse>(entity =>
            {
                entity.HasKey(e => new { e.CourseId, e.CampusCode })
                    .HasName("CI_COURSE_PK");

                entity.ToTable("CI_COURSE");

                entity.Property(e => e.CourseId)
                    .HasColumnName("COURSE_ID")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.CampusCode)
                    .HasColumnName("CAMPUS_CODE")
                    .HasColumnType("CHAR(1)");

                entity.Property(e => e.AbbreviatedTitle)
                    .HasColumnName("ABBREVIATED_TITLE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AdmissionTest1)
                    .HasColumnName("ADMISSION_TEST1")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.AdmissionTest2)
                    .HasColumnName("ADMISSION_TEST2")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.AgeRestriction)
                    .HasColumnName("AGE_RESTRICTION")
                    .HasColumnType("CHAR(1)");

                entity.Property(e => e.Attend)
                    .HasColumnName("ATTEND")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.CopyFormRequired)
                    .HasColumnName("COPY_FORM_REQUIRED")
                    .HasColumnType("CHAR(1)");

                entity.Property(e => e.CourseStructure)
                    .HasColumnName("COURSE_STRUCTURE")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CourseType)
                    .HasColumnName("COURSE_TYPE")
                    .HasColumnType("CHAR(1)");

                entity.Property(e => e.CriminalCheckRequired)
                    .HasColumnName("CRIMINAL_CHECK_REQUIRED")
                    .HasColumnType("CHAR(1)");

                entity.Property(e => e.CukasFeeExempt)
                    .HasColumnName("CUKAS_FEE_EXEMPT")
                    .HasColumnType("CHAR(1)");

                entity.Property(e => e.DeadlineDate)
                    .HasColumnName("DEADLINE_DATE")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.DegType)
                    .HasColumnName("DEG_TYPE")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Faculty)
                    .HasColumnName("FACULTY")
                    .HasColumnType("CHAR(1)");

                entity.Property(e => e.HecosCode1)
                    .HasColumnName("HECOS_CODE_1")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.HecosCode2)
                    .HasColumnName("HECOS_CODE_2")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.HecosCode3)
                    .HasColumnName("HECOS_CODE_3")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.HecosCode4)
                    .HasColumnName("HECOS_CODE_4")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.HecosCode5)
                    .HasColumnName("HECOS_CODE_5")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.HecosDescription1)
                    .HasColumnName("HECOS_DESCRIPTION_1")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.HecosDescription2)
                    .HasColumnName("HECOS_DESCRIPTION_2")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.HecosDescription3)
                    .HasColumnName("HECOS_DESCRIPTION_3")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.HecosDescription4)
                    .HasColumnName("HECOS_DESCRIPTION_4")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.HecosDescription5)
                    .HasColumnName("HECOS_DESCRIPTION_5")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.HecosPercentage1).HasColumnName("HECOS_PERCENTAGE_1");

                entity.Property(e => e.HecosPercentage2).HasColumnName("HECOS_PERCENTAGE_2");

                entity.Property(e => e.HecosPercentage3).HasColumnName("HECOS_PERCENTAGE_3");

                entity.Property(e => e.HecosPercentage4).HasColumnName("HECOS_PERCENTAGE_4");

                entity.Property(e => e.HecosPercentage5).HasColumnName("HECOS_PERCENTAGE_5");

                entity.Property(e => e.InterviewRequired)
                    .HasColumnName("INTERVIEW_REQUIRED")
                    .HasColumnType("CHAR(1)");

                entity.Property(e => e.IsFake)
                    .IsRequired()
                    .HasColumnName("IS_FAKE")
                    .HasColumnType("CHAR(1)")
                    .HasDefaultValueSql("'N'");

                entity.Property(e => e.Jacs1)
                    .HasColumnName("JACS1")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.Jacs2)
                    .HasColumnName("JACS2")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.Jacs3)
                    .HasColumnName("JACS3")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.JacsType)
                    .HasColumnName("JACS_TYPE")
                    .HasColumnType("CHAR(1)");

                entity.Property(e => e.LastUpdated)
                    .HasColumnName("LAST_UPDATED")
                    .HasColumnType("TIMESTAMP(6)");

                entity.Property(e => e.LegacyCourseCode)
                    .HasColumnName("LEGACY_COURSE_CODE")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.LetterText)
                    .HasColumnName("LETTER_TEXT")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.LocationId)
                    .HasColumnName("LOCATION_ID")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.PortfolioRequired)
                    .HasColumnName("PORTFOLIO_REQUIRED")
                    .HasColumnType("CHAR(1)");

                entity.Property(e => e.ProviderId)
                    .HasColumnName("PROVIDER_ID")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Qts)
                    .HasColumnName("QTS")
                    .HasColumnType("CHAR(1)");

                entity.Property(e => e.Qualification)
                    .HasColumnName("QUALIFICATION")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.SchemeCode)
                    .HasColumnName("SCHEME_CODE")
                    .HasColumnType("CHAR(1)");

                entity.Property(e => e.ShortTitle)
                    .HasColumnName("SHORT_TITLE")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TestRequirements).HasColumnName("TEST_REQUIREMENTS");

                entity.Property(e => e.Title)
                    .HasColumnName("TITLE")
                    .HasMaxLength(250)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CiCourseInstrument>(entity =>
            {
                entity.HasKey(e => new { e.CourseId, e.InstrumentName, e.YearCode })
                    .HasName("CI_COURSE_INSTRUMENT_PK");

                entity.ToTable("CI_COURSE_INSTRUMENT");

                entity.Property(e => e.CourseId)
                    .HasColumnName("COURSE_ID")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.InstrumentName)
                    .HasColumnName("INSTRUMENT_NAME")
                    .HasMaxLength(800)
                    .IsUnicode(false);

                entity.Property(e => e.YearCode)
                    .HasColumnName("YEAR_CODE")
                    .HasColumnType("NUMBER(4)");

                entity.Property(e => e.LastUpdated)
                    .HasColumnName("LAST_UPDATED")
                    .HasColumnType("TIMESTAMP(6)");
            });

            modelBuilder.Entity<CiCourseJointInstrument>(entity =>
            {
                entity.HasKey(e => new { e.CourseId, e.InstrumentName, e.IsPrimary, e.YearCode })
                    .HasName("CI_COURSE_JOINT_INSTRUMENT_PK");

                entity.ToTable("CI_COURSE_JOINT_INSTRUMENT");

                entity.Property(e => e.CourseId)
                    .HasColumnName("COURSE_ID")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.InstrumentName)
                    .HasColumnName("INSTRUMENT_NAME")
                    .HasMaxLength(800)
                    .IsUnicode(false);

                entity.Property(e => e.IsPrimary)
                    .HasColumnName("IS_PRIMARY")
                    .HasColumnType("CHAR(1)");

                entity.Property(e => e.YearCode)
                    .HasColumnName("YEAR_CODE")
                    .HasColumnType("NUMBER(4)");

                entity.Property(e => e.LastUpdated)
                    .HasColumnName("LAST_UPDATED")
                    .HasColumnType("TIMESTAMP(6)");
            });

            modelBuilder.Entity<CiCourseOption>(entity =>
            {
                entity.HasKey(e => new { e.OptionId, e.CourseId, e.LegacyCycleYear })
                    .HasName("CI_COURSE_OPTION_PK");

                entity.ToTable("CI_COURSE_OPTION");

                entity.HasIndex(e => e.CourseId)
                    .HasName("IDX_CI_COURSE_OPT_COURSE_ID");

                entity.Property(e => e.OptionId)
                    .HasColumnName("OPTION_ID")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.CourseId)
                    .HasColumnName("COURSE_ID")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.LegacyCycleYear)
                    .HasColumnName("LEGACY_CYCLE_YEAR")
                    .HasColumnType("NUMBER(4)");

                entity.Property(e => e.CourseStatus)
                    .HasColumnName("COURSE_STATUS")
                    .HasColumnType("CHAR(1)");

                entity.Property(e => e.DurationYears)
                    .HasColumnName("DURATION_YEARS")
                    .HasColumnType("NUMBER(3,1)");

                entity.Property(e => e.IsFake)
                    .IsRequired()
                    .HasColumnName("IS_FAKE")
                    .HasColumnType("CHAR(1)")
                    .HasDefaultValueSql("'N'");

                entity.Property(e => e.LastUpdated)
                    .HasColumnName("LAST_UPDATED")
                    .HasColumnType("TIMESTAMP(6)");

                entity.Property(e => e.LocationId)
                    .HasColumnName("LOCATION_ID")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.PublishedFlag)
                    .HasColumnName("PUBLISHED_FLAG")
                    .HasColumnType("CHAR(1)");

                entity.Property(e => e.StartDate)
                    .HasColumnName("START_DATE")
                    .HasColumnType("DATE");

                entity.Property(e => e.StartMonth)
                    .HasColumnName("START_MONTH")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.StartYear)
                    .HasColumnName("START_YEAR")
                    .HasColumnType("NUMBER(4)");

                entity.Property(e => e.VacancyExtraStatus)
                    .HasColumnName("VACANCY_EXTRA_STATUS")
                    .HasColumnType("CHAR(1)");

                entity.Property(e => e.VacancyStatus)
                    .HasColumnName("VACANCY_STATUS")
                    .HasColumnType("CHAR(1)");
            });

            modelBuilder.Entity<CiCourseStudyType>(entity =>
            {
                entity.HasKey(e => new { e.CourseId, e.YearCode })
                    .HasName("CI_COURSE_STUDY_TYPE_PK");

                entity.ToTable("CI_COURSE_STUDY_TYPE");

                entity.Property(e => e.CourseId)
                    .HasColumnName("COURSE_ID")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.YearCode)
                    .HasColumnName("YEAR_CODE")
                    .HasColumnType("NUMBER(4)");

                entity.Property(e => e.HasAlternativeStudytype)
                    .HasColumnName("HAS_ALTERNATIVE_STUDYTYPE")
                    .HasColumnType("CHAR(1)");

                entity.Property(e => e.HasJointStudytype)
                    .HasColumnName("HAS_JOINT_STUDYTYPE")
                    .HasColumnType("CHAR(1)");

                entity.Property(e => e.HasMainStudytype)
                    .HasColumnName("HAS_MAIN_STUDYTYPE")
                    .HasColumnType("CHAR(1)");

                entity.Property(e => e.HasSecondaryStudytype)
                    .HasColumnName("HAS_SECONDARY_STUDYTYPE")
                    .HasColumnType("CHAR(1)");

                entity.Property(e => e.LastUpdated)
                    .HasColumnName("LAST_UPDATED")
                    .HasColumnType("TIMESTAMP(6)");
            });

            modelBuilder.Entity<CiProvider>(entity =>
            {
                entity.HasKey(e => e.ProviderId)
                    .HasName("CI_PROVIDER_PK");

                entity.ToTable("CI_PROVIDER");

                entity.HasIndex(e => e.InstCode)
                    .HasName("IDX_CI_PROVIDER_INST_CODE");

                entity.Property(e => e.ProviderId)
                    .HasColumnName("PROVIDER_ID")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.AbbreviatedName)
                    .HasColumnName("ABBREVIATED_NAME")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.AddressLine1)
                    .HasColumnName("ADDRESS_LINE1")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AddressLine2)
                    .HasColumnName("ADDRESS_LINE2")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AddressLine3)
                    .HasColumnName("ADDRESS_LINE3")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AddressLine4)
                    .HasColumnName("ADDRESS_LINE4")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CukasProvider)
                    .HasColumnName("CUKAS_PROVIDER")
                    .HasColumnType("CHAR(1)");

                entity.Property(e => e.InstCode)
                    .HasColumnName("INST_CODE")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.LastUpdated)
                    .HasColumnName("LAST_UPDATED")
                    .HasColumnType("TIMESTAMP(6)");

                entity.Property(e => e.LiveProvider)
                    .HasColumnName("LIVE_PROVIDER")
                    .HasColumnType("CHAR(1)");

                entity.Property(e => e.Name)
                    .HasColumnName("NAME")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Postcode)
                    .HasColumnName("POSTCODE")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.Region)
                    .HasColumnName("REGION")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.ShortName)
                    .HasColumnName("SHORT_NAME")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UcasProvider)
                    .HasColumnName("UCAS_PROVIDER")
                    .HasColumnType("CHAR(1)");

                entity.Property(e => e.Ukprn)
                    .HasColumnName("UKPRN")
                    .HasColumnType("NUMBER(8)");
            });

            modelBuilder.Entity<CiProviderContact>(entity =>
            {
                entity.HasKey(e => new { e.ContactId, e.ProviderId })
                    .HasName("CI_PROVIDER_CONTACT_PK");

                entity.ToTable("CI_PROVIDER_CONTACT");

                entity.Property(e => e.ContactId)
                    .HasColumnName("CONTACT_ID")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.ProviderId)
                    .HasColumnName("PROVIDER_ID")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasColumnName("EMAIL")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Fax)
                    .HasColumnName("FAX")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Isdefault)
                    .HasColumnName("ISDEFAULT")
                    .HasColumnType("CHAR(1)");

                entity.Property(e => e.LastUpdated)
                    .HasColumnName("LAST_UPDATED")
                    .HasColumnType("TIMESTAMP(6)");

                entity.Property(e => e.Phone)
                    .HasColumnName("PHONE")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Title)
                    .HasColumnName("TITLE")
                    .HasMaxLength(150)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CiProviderFee>(entity =>
            {
                entity.HasKey(e => new { e.InstCode, e.YearCode })
                    .HasName("CI_PROVIDER_FEE_PK");

                entity.ToTable("CI_PROVIDER_FEE");

                entity.Property(e => e.InstCode)
                    .HasColumnName("INST_CODE")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.YearCode)
                    .HasColumnName("YEAR_CODE")
                    .HasColumnType("NUMBER(4)");

                entity.Property(e => e.LastUpdated)
                    .HasColumnName("LAST_UPDATED")
                    .HasColumnType("TIMESTAMP(6)");

                entity.Property(e => e.NumberSecondary)
                    .HasColumnName("NUMBER_SECONDARY")
                    .HasColumnType("NUMBER(2)");
            });

            modelBuilder.Entity<CiProviderLocation>(entity =>
            {
                entity.HasKey(e => new { e.LocationId, e.ProviderId })
                    .HasName("CI_PROVIDER_LOCATION_PK");

                entity.ToTable("CI_PROVIDER_LOCATION");

                entity.HasIndex(e => e.ProviderId)
                    .HasName("IDX_CI_PROV_LOC_PROVIDER_ID");

                entity.Property(e => e.LocationId)
                    .HasColumnName("LOCATION_ID")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.ProviderId)
                    .HasColumnName("PROVIDER_ID")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.CampusCode)
                    .HasColumnName("CAMPUS_CODE")
                    .HasColumnType("CHAR(1)");

                entity.Property(e => e.LastUpdated)
                    .HasColumnName("LAST_UPDATED")
                    .HasColumnType("TIMESTAMP(6)");

                entity.Property(e => e.Name)
                    .HasColumnName("NAME")
                    .HasMaxLength(250)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MapAttAdmissionTestType>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("MAP_ATT_ADMISSION_TEST_TYPE");

                entity.Property(e => e.AdmissionTestCode)
                    .HasColumnName("ADMISSION_TEST_CODE")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.AdmissionTestTypeId)
                    .HasColumnName("ADMISSION_TEST_TYPE_ID")
                    .HasMaxLength(2)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MapCourseType>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("MAP_COURSE_TYPE");

                entity.Property(e => e.Code)
                    .HasColumnName("CODE")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.CollectId)
                    .HasColumnName("COLLECT_ID")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .HasColumnName("DESCRIPTION")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MapQltQlfType>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("MAP_QLT_QLF_TYPE");

                entity.Property(e => e.CollectDescription)
                    .HasColumnName("COLLECT_DESCRIPTION")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.CollectId).HasColumnName("COLLECT_ID");

                entity.Property(e => e.StructureCode)
                    .HasColumnName("STRUCTURE_CODE")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.TopazFullDescription)
                    .HasColumnName("TOPAZ_FULL_DESCRIPTION")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.TopazShortDescription)
                    .HasColumnName("TOPAZ_SHORT_DESCRIPTION")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.TrfQltId).HasColumnName("TRF_QLT_ID");
            });

            modelBuilder.Entity<MapRegRegion>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("MAP_REG_REGION");

                entity.Property(e => e.AmsCode)
                    .HasColumnName("AMS_CODE")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.AmsDescription)
                    .HasColumnName("AMS_DESCRIPTION")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Code)
                    .HasColumnName("CODE")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .HasColumnName("DESCRIPTION")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.McsUrnId).HasColumnName("MCS_URN_ID");

                entity.Property(e => e.RegId).HasColumnName("REG_ID");
            });

            modelBuilder.Entity<MapStmStudyMode>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("MAP_STM_STUDY_MODE");

                entity.Property(e => e.CollectId)
                    .HasColumnName("COLLECT_ID")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FtptFlag)
                    .HasColumnName("FTPT_FLAG")
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .HasDefaultValueSql("'PT'");

                entity.Property(e => e.StudyModeCode)
                    .HasColumnName("STUDY_MODE_CODE")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.StudyModeCodePg)
                    .HasColumnName("STUDY_MODE_CODE_PG")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.StudyModeType)
                    .HasColumnName("STUDY_MODE_TYPE")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
