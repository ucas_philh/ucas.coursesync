﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ucas.CourseSync.CourseIntegration.Db.Context;
using Ucas.CourseSync.CourseIntegration.Db.Entities;

namespace Ucas.CourseSync.CourseIntegration.Db.Repositories
{
    public class ReferenceDataRepository : IReferenceDataRepository
    {
        private readonly CourseIntegrationContext _context;

        public ReferenceDataRepository(CourseIntegrationContext context)
        {
            _context = context;
        }

        public Task<MapAttAdmissionTestType> AdmissionTest(string id)
        {
            return Task.Run(() =>
            {
                var result = _context.MapAttAdmissionTestType.Where(m => m.AdmissionTestTypeId == id).FirstOrDefault();
                return result;
            });
        }

        public Task<MapCourseType> CourseType(string id)
        {
            return Task.Run(() =>
            {
                var result = _context.MapCourseType.Where(m => m.CollectId == id).FirstOrDefault();
                return result;
            });
        }

        public Task<MapQltQlfType> OutcomeQualification(int id)
        {
            return Task.Run(() =>
            {
                var result = _context.MapQltQlfType.Where(m => m.CollectId == id).FirstOrDefault();
                return result;
            });
        }

        public Task<MapRegRegion> Region(string id)
        {
            return Task.Run(() =>
            {
                var result = _context.MapRegRegion.Where(m => m.AmsCode == id).FirstOrDefault();
                return result;
            });
        }

        public Task<MapStmStudyMode> StudyMode(string id)
        {
            return Task.Run(() =>
            {
                var result = _context.MapStmStudyMode.Where(m => m.CollectId == id).FirstOrDefault();
                return result;
            });
        }
    }
}
