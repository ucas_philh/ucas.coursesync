﻿using System.Threading.Tasks;
using Ucas.CourseSync.CourseIntegration.Db.Entities;

namespace Ucas.CourseSync.CourseIntegration.Db.Repositories
{
    public interface IReferenceDataRepository
    {
        Task<MapAttAdmissionTestType> AdmissionTest(string id);
        Task<MapCourseType> CourseType(string id);
        Task<MapQltQlfType> OutcomeQualification(int id);
        Task<MapRegRegion> Region(string id);
        Task<MapStmStudyMode> StudyMode(string id);
    }
}