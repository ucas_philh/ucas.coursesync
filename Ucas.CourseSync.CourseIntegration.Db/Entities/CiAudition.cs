﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Ucas.CourseSync.CourseIntegration.Db.Entities
{
    public partial class CiAudition
    {
        public string CourseId { get; set; }
        public string AuditionMode { get; set; }
        public string CukasCollectFlag { get; set; }
        public decimal? FeeOntime1 { get; set; }
        public decimal? FeeOntime2 { get; set; }
        public decimal? FeeLate1 { get; set; }
        public decimal? FeeLate2 { get; set; }
        public DateTime? LastUpdated { get; set; }
    }
}
