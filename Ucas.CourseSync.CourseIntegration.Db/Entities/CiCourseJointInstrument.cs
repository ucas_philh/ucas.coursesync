﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Ucas.CourseSync.CourseIntegration.Db.Entities
{
    public partial class CiCourseJointInstrument
    {
        public string CourseId { get; set; }
        public string InstrumentName { get; set; }
        public string IsPrimary { get; set; }
        public byte YearCode { get; set; }
        public DateTime? LastUpdated { get; set; }
    }
}
