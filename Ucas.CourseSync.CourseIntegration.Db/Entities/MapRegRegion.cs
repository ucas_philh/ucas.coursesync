﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Ucas.CourseSync.CourseIntegration.Db.Entities
{
    public partial class MapRegRegion
    {
        public int? RegId { get; set; }
        public int? McsUrnId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string AmsCode { get; set; }
        public string AmsDescription { get; set; }
    }
}
