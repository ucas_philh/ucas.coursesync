﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Ucas.CourseSync.CourseIntegration.Db.Entities
{
    public partial class CiProvider
    {
        public string ProviderId { get; set; }
        public string InstCode { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string AbbreviatedName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string AddressLine4 { get; set; }
        public string Postcode { get; set; }
        public string Region { get; set; }
        public string UcasProvider { get; set; }
        public string CukasProvider { get; set; }
        public string LiveProvider { get; set; }
        public int? Ukprn { get; set; }
        public DateTime? LastUpdated { get; set; }
    }
}
