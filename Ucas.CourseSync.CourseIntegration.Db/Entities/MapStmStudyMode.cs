﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Ucas.CourseSync.CourseIntegration.Db.Entities
{
    public partial class MapStmStudyMode
    {
        public string CollectId { get; set; }
        public string StudyModeType { get; set; }
        public string StudyModeCode { get; set; }
        public string StudyModeCodePg { get; set; }
        public string FtptFlag { get; set; }
    }
}
