﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Ucas.CourseSync.CourseIntegration.Db.Entities
{
    public partial class CiCourse
    {
        public string CourseId { get; set; }
        public string ProviderId { get; set; }
        public string LocationId { get; set; }
        public string CampusCode { get; set; }
        public string LegacyCourseCode { get; set; }
        public string SchemeCode { get; set; }
        public string Faculty { get; set; }
        public string Title { get; set; }
        public string ShortTitle { get; set; }
        public string AbbreviatedTitle { get; set; }
        public string Qts { get; set; }
        public string Jacs1 { get; set; }
        public string Jacs2 { get; set; }
        public string Jacs3 { get; set; }
        public string JacsType { get; set; }
        public string AgeRestriction { get; set; }
        public string LetterText { get; set; }
        public string AdmissionTest1 { get; set; }
        public string AdmissionTest2 { get; set; }
        public bool? TestRequirements { get; set; }
        public string CourseType { get; set; }
        public string DeadlineDate { get; set; }
        public string PortfolioRequired { get; set; }
        public string InterviewRequired { get; set; }
        public string CriminalCheckRequired { get; set; }
        public string CopyFormRequired { get; set; }
        public string CourseStructure { get; set; }
        public string DegType { get; set; }
        public string Qualification { get; set; }
        public DateTime LastUpdated { get; set; }
        public string Attend { get; set; }
        public string CukasFeeExempt { get; set; }
        public string HecosCode1 { get; set; }
        public string HecosDescription1 { get; set; }
        public byte? HecosPercentage1 { get; set; }
        public string HecosCode2 { get; set; }
        public string HecosDescription2 { get; set; }
        public byte? HecosPercentage2 { get; set; }
        public string HecosCode3 { get; set; }
        public string HecosDescription3 { get; set; }
        public byte? HecosPercentage3 { get; set; }
        public string HecosCode4 { get; set; }
        public string HecosDescription4 { get; set; }
        public byte? HecosPercentage4 { get; set; }
        public string HecosCode5 { get; set; }
        public string HecosDescription5 { get; set; }
        public byte? HecosPercentage5 { get; set; }
        public string IsFake { get; set; }
    }
}
