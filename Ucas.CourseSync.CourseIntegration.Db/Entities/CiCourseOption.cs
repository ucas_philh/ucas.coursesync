﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Ucas.CourseSync.CourseIntegration.Db.Entities
{
    public partial class CiCourseOption
    {
        public string OptionId { get; set; }
        public string CourseId { get; set; }
        public string LocationId { get; set; }
        public byte LegacyCycleYear { get; set; }
        public DateTime? StartDate { get; set; }
        public string StartMonth { get; set; }
        public byte? StartYear { get; set; }
        public decimal? DurationYears { get; set; }
        public string PublishedFlag { get; set; }
        public string CourseStatus { get; set; }
        public string VacancyStatus { get; set; }
        public string VacancyExtraStatus { get; set; }
        public DateTime? LastUpdated { get; set; }
        public string IsFake { get; set; }
    }
}
