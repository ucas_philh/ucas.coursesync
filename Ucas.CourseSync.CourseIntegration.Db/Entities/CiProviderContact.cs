﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Ucas.CourseSync.CourseIntegration.Db.Entities
{
    public partial class CiProviderContact
    {
        public string ContactId { get; set; }
        public string ProviderId { get; set; }
        public string Title { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Isdefault { get; set; }
        public DateTime? LastUpdated { get; set; }
    }
}
