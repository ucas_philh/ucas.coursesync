﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Ucas.CourseSync.CourseIntegration.Db.Entities
{
    public partial class CiProviderLocation
    {
        public string LocationId { get; set; }
        public string ProviderId { get; set; }
        public string CampusCode { get; set; }
        public string Name { get; set; }
        public DateTime? LastUpdated { get; set; }
    }
}
