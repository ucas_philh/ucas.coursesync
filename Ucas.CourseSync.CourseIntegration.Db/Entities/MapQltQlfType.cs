﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Ucas.CourseSync.CourseIntegration.Db.Entities
{
    public partial class MapQltQlfType
    {
        public int? CollectId { get; set; }
        public string CollectDescription { get; set; }
        public int? TrfQltId { get; set; }
        public string TopazFullDescription { get; set; }
        public string TopazShortDescription { get; set; }
        public string StructureCode { get; set; }
    }
}
